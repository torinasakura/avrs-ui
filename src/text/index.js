import Text from './Text'
import Space from './Space'
import Price from './Price'

export {
  Text,
  Space,
  Price,
}
